module.exports = (inventory, id) => {
  const data =[];

  /* this condition will return an empty array  
     if we got nothing or an empty array or object
  */ 
  if(inventory==undefined || typeof(inventory) != 'object' || inventory.length==undefined || inventory.length==0){
    return data;
  }

  if(inventory[0].id==undefined){
    return data;
  }

  for(let i=0; i<inventory.length; i++){
    if(inventory[i].id == id){
      data.push(inventory[i]);
    }
  }
  return data;
}