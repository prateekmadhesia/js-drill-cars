module.exports= (inventory) => {
  const BMW_Audi=[];

  /* this condition will return an empty array  
     if we got nothing or an empty array or object
  */  
  if(inventory==undefined || typeof(inventory) != 'object' || inventory.length==undefined || inventory.length==0 ){
    return BMW_Audi;
  }
  
  for(let i=0; i<inventory.length; i++){
    let car=inventory[i].car_make;
    if( car=="BMW" || car=="Audi" ){
      BMW_Audi.push(inventory[i]);
    }
  }

  return BMW_Audi;
}