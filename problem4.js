module.exports = (inventory) => {
  const carModel=[];

  /* this condition will return an empty array  
     if we got nothing or an empty array or object
  */ 
  if(inventory==undefined || typeof(inventory) != 'object' || inventory.length==undefined || inventory.length==0){
    return carModel;
  }
  
  for(let i=0; i<inventory.length; i++){
    carModel.push(inventory[i].car_year);
  }
  
  return carModel;
}