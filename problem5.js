module.exports = (inventory) => {
  const oldCar=[];

  /* this condition will return an empty array  
     if we got nothing or an empty array or object
  */ 
  if(inventory==undefined || typeof(inventory) != 'object' || inventory.length==undefined || inventory.length==0){
    return oldCar;
  }
 
   
  for(let i=0; i<inventory.length; i++){
    if( inventory[i] <2000){
      oldCar.push(inventory[i]);
    }
  }

  return oldCar;
}