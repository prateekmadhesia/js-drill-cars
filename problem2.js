module.exports = (inventory) => {
  const data =[];

  /* this condition will return an empty array  
     if we got nothing or an empty array or object
  */  
  if(inventory==undefined || typeof(inventory) != 'object' || inventory.length==undefined || inventory.length==0){
    return data;
  }

  const last =inventory.length - 1;
  data.push(inventory[last]);
  return data;
}